<div class="page-holder">
            <!-- Navbar -->
            <header class="header">
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header"><a href="index.html" class="navbar-brand"><img src="img/logo.png" alt="Italiano" width="100"></a>
                            <div class="navbar-buttons">
                                <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
                            </div>
                        </div>
                        <div id="navigation" class="collapse navbar-collapse navbar-right">
                            <ul class="nav navbar-nav">
                            <?php
                                $dir = scandir("/home/Albert_Nicolas/ServeurWeb/restaurant/contents");
                                    foreach ($dir as $files){
                                        if ($files !== "." && $files !== ".."){
                                            if(strpos($files,"_") !== FALSE){
                                                $modif = ucfirst(str_replace('_', ' ', implode('.',explode(".",substr_replace($files, "'", strrpos($files,"_",-1), 0),-1))));
                                                $done = substr_replace($modif, "", strrpos($modif," ", -1),0);
                                                echo '<li><a href="http://localhost/serveurweb/restaurant/?contents='.implode('.',explode(".",$files,-1)).'">'.$done.'</a></li>';
                                            }else{
                                                echo '<li><a href="http://localhost/serveurweb/restaurant/?contents=' . implode('.',explode(".",$files,-1)) . '">' . ucfirst(implode('.',explode(".",$files,-1))) . '</a></li>';
                                            }
                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header><!-- End Navbar -->
