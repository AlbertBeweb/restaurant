<?php require_once("./scripts/functions.php"); ?>
<?php require_once("./includes/header.php"); ?>
<?php 
    session_start();

    $users = "/home/Albert_Nicolas/ServeurWeb/restaurant/datas/users.json";
    
    $oldUsers = file_get_contents($users);
    
    $usersDecod = json_decode($oldUsers, TRUE);

    $usersTableau = array(
        
        'users'    => $_POST['users'],
        'password' => $_POST['password']
    );
    
    
    array_push($usersDecod,$usersTableau);

    $usersEncode = json_encode($usersDecod);

    file_put_contents($users,$usersEncode);
           
?>

<body>

<hr>
<br>
<div class="container">
<div class="form-gap">
</div>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="text-center">
              <h2 class="text-center">S'identifier</h2>
							<div class="panel-body">
                <form id="login-form" role="form" autocomplete="off" class="form" method="post" action="./scripts/users.php">
                  <div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                        <input name="users" type="text" class="form-control" placeholder="Identifiant">
										</div>
									</div>
                  <div class="form-group">
										<div class="input-group">
											<span class="input-group-addon"><i class="glyphicon glyphicon-lock color-blue"></i></span>
											<input name="password" type="password" class="form-control" placeholder="Mot de passe">
										</div>
									</div>
                  <div class="form-group">
                    <input name="login" class="btn btn-lg btn-primary btn-block" value="Se connecter" type="submit">
									</div>
                </form>
              </div><!-- Body-->
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> <!-- /.container -->
